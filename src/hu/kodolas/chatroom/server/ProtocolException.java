package hu.kodolas.chatroom.server;


public class ProtocolException extends Exception {

    public ProtocolException(String msg) {
        super(msg);
    }
    
    public ProtocolException(Throwable cause) {
        super(cause);
    }
}
