package hu.kodolas.chatroom.server;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final int ACTION_SOCKET_PORT = 8082;
    private static final int EVENT_SOCKET_PORT = 8083;
    private static final int TIMEOUT = 60 * 1000;

    private static ServerSocket actionServerSocket;
    private static final Set<ActionHandler> actionHandlers
            = new HashSet<>();

    private static ServerSocket eventServerSocket;
    private static final Set<EventHandler> eventHandlers
            = new HashSet<>();

    public static void main(String[] args) throws IOException {
        actionServerSocket = new ServerSocket(ACTION_SOCKET_PORT);
        actionServerSocket.setSoTimeout(0);
        eventServerSocket = new ServerSocket(EVENT_SOCKET_PORT);
        eventServerSocket.setSoTimeout(0);

        waitForEventSockets();
        waitForActionSockets();
    }

    public static synchronized void actionHandlerDisconnected(
            ActionHandler handler) {
        actionHandlers.remove(handler);
        broadcastOnlineUsersChanged();
    }

    public static synchronized void eventHandlerDisconnected(
            EventHandler handler) {
        eventHandlers.remove(handler);
    }

    public static void broadcastNewMessage(int senderId, int msgId,
            String msg) {
        broadcastMessage(
                EventHandler.createSendMessage(senderId, msgId, msg));
    }

    public static void broadcastOnlineUsersChanged() {
        broadcastMessage(EventHandler.createOnlineUsersChangedMessage());
    }
    
    public static void broadcastShakeWindow() {
        broadcastMessage(EventHandler.createShakeWindowMessage());
    }

    private static synchronized void broadcastMessage(JsonObject msg) {
        for (EventHandler handler : new HashSet<>(eventHandlers)) { // Must copy to avoid ConcurrentModificationException
            try {
                handler.sendMessage(msg);
            } catch (IOException e) {
                try {
                    handler.close();
                    System.err.println(e);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public static synchronized Map<Integer, String> getOnlineUsers() {
        Map<Integer, String> out = new HashMap<>();

        for (ActionHandler handler : actionHandlers) {
            out.put(handler.id,
                    handler.getName() == null ? "???" : handler.getName());

        }
        return out;
    }

    private static void waitForActionSockets() {
        while (true)
            waitForActionSocket();
    }

    private static void waitForEventSockets() {
        Thread t = new Thread("Event socket listener") {
            @Override
            public void run() {
                while (true)
                    waitForEventSocket();
            }

        };
        t.setDaemon(true);
        t.start();
    }

    private static void waitForActionSocket() {

        System.out.println("Waiting for action sockets on port "
                + actionServerSocket
                        .getLocalPort() + "...");
        try {
            Socket socket = actionServerSocket.accept();
            synchronized (Main.class) {
                ActionHandler newHandler = new ActionHandler(socket);
                actionHandlers.add(newHandler);
            }

        } catch (IOException e) {
            System.out.println("Action Socket timed out!");
        }
    }

    private static void waitForEventSocket() {

        System.out.println("Waiting for event sockets on port "
                + eventServerSocket
                        .getLocalPort() + "...");
        try {
            Socket socket = eventServerSocket.accept();
            synchronized (Main.class) {
                EventHandler newHandler = new EventHandler(socket);
                eventHandlers.add(newHandler);
            }

        } catch (IOException e) {
            System.out.println("Event Socket timed out!");
        }
    }

}
