package hu.kodolas.chatroom.server;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;

public class ActionHandler implements Closeable {

    private enum State {
        WAITING_FOR_LOGIN, LOGGED_IN
    }

    private static final String ENCODING = "UTF-8";
    private static int idCounter = 0;

    public final int id;
    private String name;
    private int nextMsgid = 0;

    private final Object READ_LOCK = new Object();
    private final Object WRITE_LOCK = new Object();

    private final Socket socket;
    private Reader in;
    private Writer out;
    private JsonStreamParser jsonParser;

    private State state = State.WAITING_FOR_LOGIN;

    public ActionHandler(Socket socket) throws IOException {
        this.socket = socket;
        startThread();
        id = idCounter++; // Called only from main thread
    }

    public String getName() {
        return name;
    }

    private synchronized void startThread() {
        Thread t = new Thread() {
            @Override
            public void run() {
                init();
                doTask();
            }

        };
        t.setDaemon(true);
        t.start();
    }

    private synchronized void init() {
        try {
            System.out.println("Action socket connected to " + socket
                    .getRemoteSocketAddress());
            in = new InputStreamReader(socket.getInputStream(), ENCODING);
            out = new OutputStreamWriter(socket.getOutputStream(), ENCODING);
            jsonParser = new JsonStreamParser(in);

        } catch (IOException e) {
            System.err.println("Could not init");
            e.printStackTrace();
            Main.actionHandlerDisconnected(this);
        }
    }

    public synchronized void close() {
        try {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
            socket.close();
            Main.actionHandlerDisconnected(this);
            System.out.println("Disconnected from " + socket
                    .getRemoteSocketAddress());

        } catch (IOException ex) {
            System.err.println("Could not close socket properly");
            ex.printStackTrace();
        }
    }

    private void doTask() {

        try {
            while (true) {
                execute(readJSON());
            }
        } catch (Throwable t) {
            try {
                synchronized (this) {
                    sendMessage(createFailResponse(t));
                }
            } catch (IOException ex) {
                //ex.printStackTrace();
            }
            //t.printStackTrace();

        } finally {
            close();
        }
    }

    private JsonObject readJSON() throws IOException {
        synchronized (READ_LOCK) {
            return jsonParser.next().getAsJsonObject();
        }
    }

    private synchronized void execute(JsonObject obj) throws ProtocolException,
            IOException {
        String action = obj.get("action").getAsString();
        JsonObject response;
        switch (action) {
            case "login":
                response = executeLogin(obj);
                break;
            case "send":
                response = executeSend(obj);
                break;
            case "onlineusers":
                response = executeOnlineUsers(obj);
                break;
            case "shakewindow":
                response = executeShakeWindow(obj);
                break;
            default:
                throw new ProtocolException("Unknown command");
        }
        sendMessageIfLoggedIn(response);
    }

    public void sendMessageIfLoggedIn(JsonObject obj) throws
            IOException {
        if (state != State.LOGGED_IN)
            return;
        sendMessage(obj);
    }

    private void sendMessage(JsonObject obj) throws IOException {
        synchronized (WRITE_LOCK) {
            out.append(obj.toString());
            out.append('\n');
            out.flush();
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Login 
    ////////////////////////////////////////////////////////////////////////////
    private JsonObject executeLogin(JsonObject obj) throws ProtocolException,
            IOException {
        if (state != State.WAITING_FOR_LOGIN)
            throw new ProtocolException("Already loggged in");

        name = obj.get("name").getAsString();
        System.out.println("Successful login. id: " + id + " name: '" + name
                + "'");
        state = State.LOGGED_IN;

        Main.broadcastOnlineUsersChanged();

        return createLoginSuccessResponse();
    }

    private JsonObject createLoginSuccessResponse() throws IOException {
        JsonObject obj = createSuccessResponse();
        obj.addProperty("id", id);
        return obj;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Send
    ////////////////////////////////////////////////////////////////////////////
    private JsonObject executeSend(JsonObject obj) throws IOException,
            ProtocolException {
        checkLoggedIn();
        checkId(obj);
        int msgid = obj.get("msgid").getAsInt();
        if (msgid != nextMsgid)
            throw new ProtocolException("Expected msgid is " + nextMsgid
                    + " not " + msgid);
        ++nextMsgid;
        String msg = obj.get("message").getAsString();
        Main.broadcastNewMessage(id, msgid, msg);
        System.out.println("Message from: "+name+": "+msg);
        return createSuccessResponse();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Shake window
    ////////////////////////////////////////////////////////////////////////////
    private JsonObject executeShakeWindow(JsonObject obj) throws IOException,
            ProtocolException {
        checkLoggedIn();
        checkId(obj);
        
        Main.broadcastShakeWindow();
        return createSuccessResponse();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Online users
    ////////////////////////////////////////////////////////////////////////////
    private JsonObject executeOnlineUsers(JsonObject obj) throws
            ProtocolException, IOException {
        checkLoggedIn();
        checkId(obj);
        return createOnlineUsersResponse();
    }

    private JsonObject createOnlineUsersResponse() throws IOException {
        JsonObject obj = createSuccessResponse();
        Map<Integer, String> map = Main.getOnlineUsers();
        JsonArray onlineUsers = new JsonArray();

        for (Entry<Integer, String> entry : map.entrySet()) {
            JsonObject onlineUser = new JsonObject();
            onlineUser.addProperty("id", entry.getKey());
            onlineUser.addProperty("name", entry.getValue());
            onlineUsers.add(onlineUser);
        }
        obj.add("onlineusers", onlineUsers);

        return obj;
    }

    ////////////////////////////////////////////////////////////////////////////
    // General responses
    ////////////////////////////////////////////////////////////////////////////
    private JsonObject createSuccessResponse() throws IOException {
        JsonObject obj = new JsonObject();
        obj.addProperty("success", Boolean.TRUE);
        return obj;
    }

    private JsonObject createFailResponse(Throwable t) throws IOException {
        JsonObject obj = new JsonObject();
        obj.addProperty("success", Boolean.FALSE);
        obj.addProperty("message", t.getMessage());
        return obj;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Util methods 
    ////////////////////////////////////////////////////////////////////////////
    private void checkLoggedIn() throws ProtocolException {
        if (state != State.LOGGED_IN)
            throw new ProtocolException(
                    "You can execute this command only if you are logged in");
    }

    private void checkId(JsonObject obj) throws ProtocolException {
        int check = obj.get("id").getAsInt();
        if (check != id)
            throw new ProtocolException("Ids does not match. Expected: " + id
                    + " get: " + check);
    }

}
