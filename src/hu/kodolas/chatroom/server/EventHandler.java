package hu.kodolas.chatroom.server;

import com.google.gson.JsonObject;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

public class EventHandler implements Closeable {

    private final Socket socket;
    private final Writer out;

    public EventHandler(Socket socket) throws IOException {
        System.out
                .println("Event socket connected to " + socket
                        .getRemoteSocketAddress());
        this.socket = socket;
        out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
    }

    public synchronized void sendMessage(JsonObject message) throws IOException {
        out.write(message.toString());
        out.flush();
    }

    @Override
    public synchronized void close() throws IOException {
        if (out != null)
            out.close();
        socket.close();
        Main.eventHandlerDisconnected(this);
    }

    public static JsonObject createSendMessage(int senderId,
            int msgId, String msg) {
        JsonObject obj = new JsonObject();
        obj.addProperty("eventtype", "newmessage");
        obj.addProperty("senderid", senderId);
        obj.addProperty("msgid", msgId);
        obj.addProperty("message", msg);
        return obj;
    }

    public static JsonObject createOnlineUsersChangedMessage() {
        JsonObject obj = new JsonObject();
        obj.addProperty("eventtype", "onlineuserschanged");
        return obj;
    }
    
    public static JsonObject createShakeWindowMessage() {
        JsonObject obj = new JsonObject();
        obj.addProperty("eventtype", "shakewindow");
        return obj;
    }
}
